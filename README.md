# **A Collection of Tweaks applied to my Void Linux Musl System.**


 Though I am taking it to work for two days now, I'm not sure if it's ready yet. Anyways, here are my applied tweaks and some battery benchmarks. The setup is on default powertop + tlp config and I'm yet to apply additional conserving options.


## SYSCTL
* Make a folder called sysctl.d in `/etc` if it doesn't exist yet and place all the configs there.

## KERNEL
* Make a folder inside your ESP (Mine is mounted at /boot, do either `mount` or `df -h` if you're unsure).
* Let's call that folder EFISTUB. Safely place the kernel inside that folder and either use efibootmgr to create an UEFI entry.
    * As it happens in my case, the UEFI on my Inspiron 7577 allows additions through the interface. If you're on a relatively new Dell
      workstation, it might be the same for you.
* Lastly, make sure you set the priority right and boot into it.


Mine currently boots in under 10s without `startx` and 12 if you count `startx`. A majority of boot time seems be spent in intializing firmware while the POST option from DELL, changing it seems to make no change. Currently investing whatever time I have left after work in building further trimmed down and customized kernels, and of course, getting the boot time to <= 5s.
