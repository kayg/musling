#!/usr/bin/env bash

cd "$HOME" || exit 1
git clone https://github.com/gpakosz/.tmux.git
ln -sf "$HOME/.tmux/.tmux.conf" "$HOME/"
cp "$HOME/.tmux/.tmux.conf.local" "$HOME/"

exit 0
