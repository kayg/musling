#!/usr/bin/env bash

function run {
	if ! pgrep $1;
	then
		$@&
	fi
}

run xrdb -merge $HOME/.Xresources
run udiskie --tray
run compton -bf
run solaar
